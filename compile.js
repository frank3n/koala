const {promises} = require('fs');
const pages = require('./pages/pages.json');
const posts = require('./blog/posts/posts.json');

const template = 'template/template.html';
const blogTemplate = './blog/template/template.html';
const encoding = 'UTF-8';

const processTemplates = async () => {
  pages.forEach(async page => {
    try {
      const templateContent = await promises.readFile(template, {encoding});
      const content = await promises.readFile(`./pages/${page.file}`, {encoding});
      promises.writeFile(
        page.file,
        templateContent
          .replace('{{template}}', content)
          .replace('{{title}}', page.title)
          .replace('{{description}}', page.description),
        {encoding});
    } catch (e) {
      console.error(e);
    }
  });
};

const processBlog = async () => {
  posts.forEach(async page => {
    try {
      const templateContent = await promises.readFile(blogTemplate, {encoding});
      const content = await promises.readFile(`./blog/posts/${page.file}`, {encoding});
      promises.writeFile(
        `./blog/${page.file}`,
        templateContent
          .replace('{{template}}', content)
          .replace('{{title}}', page.title)
          .replace('{{description}}', page.description),
        {encoding});
    } catch (e) {
      console.error(e);
    }
  });
};

try {
  processTemplates();
  processBlog();
} catch (e) {
  console.error(e);
}
