# Konfiguracja

1. Do odpalenia skryptu wymagany jest zainstalowany node.js wersja > 11.4.0
(https://nodejs.org/en/download/current/)
2. Po zainstalowaniu node.js, nalezy z poziomu linii polecen dostac sie do
glownego katalogu projektu, np. C:\koala-kombucha lub ~/koala-kombucha
3. Z poziomu linii polecen w glownym katalogu projektu nalezy wykonac `npm install`
4. Z poziomu linii polecen w glownym katalogu projektu nalezy wykonac `npm run watch`

# Wprowadzanie zmian

Plik `/pages/pages.json` opisuje strony, ktore zostana wygenerowane z uzyciem szablonu
znajdujacego sie w folderze `template/template.html` oraz plikow z contentem znadujacych sie w folderze `/pages`.

Kazdy wpis w pliku `/pages/pages.json` zawiera:
- `title` - docelowy content dla tagu `<title></title>`
- `description` - docelowy content dla wartosci tagu `<meta name="description" />`
- `name` - nazwa pliku z contentem, ktory ma byc uzyty to wygenerowania strony statycznej

Kazda ze stron w folderze `/pages` zawiera tylko cialo docelowo wygenerowanej strony statycznej,
ktore zostanie wstrzykniete do pliku `template/template.html` w miejscu tagu `{{template}}`.

Strony statyczne sa generowane automatycznie w katalogu glownym projektu po zapisaniu jakichkolwiek zmian.

# Podsumowanie

Po zainstalowaniu node.js w katalogu glownym projektu, trzeba wykonac komende
`npm run watch`, po czym pozostawic w tle otwarta linie polecen.

Zeby przykladowo zmienic zawartosc strony glownej, wystarczy dokonac zmian
w pliku `pages/index.html`, po czym zapisac zmiany. W katalogu glownym
projektu zostanie wygenerowany nowy plik `index.html` z naniesionymi zmianami.

Aby dokonac zmian w szablonie, wystarczy analogicznie zmienic cos w pliku
`template/template.html` i zapisac zmiany. Wszystkie pliki `.html` w katalogu glownym
projektu zostana przegenerowane.

# Dodawanie nowych stron

Zeby dodac nowa strone, nalezy dodac nowy plik .html do folderu `/pages` oraz
dodac wpis do pliku `/pages/pages.json`.

Przykladowo, zeby dodac nowa strone `test.html`, nalezy dokonac zmian w pliku `/pages/pages.json`:

```
[{
  "file": "index.html",
  "description": "Kombucha, inaczej zwana kombucza jest naturalnym probiotykiem bez zawartości cukru. Nasz sklep oferuje kombuczę z mieszanki organicznych herbat. Naturalny i niepasteryzowany napój z żywymi kulturami bakrerii oraz antyoksydantami. Sprawdź gdzie kupić kombuchę!",
  "title": "Koala Kombucha - Kombucza - sklep - gdzie kupić"
}, {
  "file": "dieta-kombucha.html",
  "title": "Koala Kombucha - Dieta",
  "description": "Dieta na jakiej można spożywać probiotyk Koala Kombucha"
}, {
  "file": "o-koali-kombucha.html",
  "title": "Koala Kombucha - O nas!",
  "description": "Informacje o nas i naszym produkcie - Koala Kombucha"
}, {
   "file": "test.html",
   "title": "Testowa wartosc tagu <title> na nowej stronie test.html",
   "description": "Wartosc atrybutu conten= w tagu <meta name='description'"
 }]
```

nastepnie nalezy dodac plik `test.html` do katalogu `/pages`.
Po dokonaniau tych zmian zostanie wygenerowana nowa strona statyczna `test.html` w katalogu
glownym projektu.
