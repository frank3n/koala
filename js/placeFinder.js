(function(mapboxgl) {
  mapboxgl.accessToken = 'pk.eyJ1IjoiZnJhbmszbiIsImEiOiJjam9heDdqa2wwZHg5M3FvemJ1MTRzanVpIn0.ChoxEAv4zZayTEK5Ddn8IQ';

  const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    center: [17.552812, 51.438544],
    zoom: 5.15,
    pitch: 35,
    bearing: -17
  });

  const places = [
    {
      coordinates: {lat: 53.402243, lng: 14.532756},
      info: 'Koala Kombucha - czynne w każdą niedzielę od 10 do 15, Szczeciński Bazar Smakoszy, Zygmunta Chmielewskiego 18, 70-001 Szczecin',
      logo: 'koala-no-bg.png'
    },
    {
      coordinates: {lat: 53.43153, lng: 14.54569},
      info: 'Artetamina - Generała Ludomiła Rayskiego 21, 71-064 Szczecin'
    },
    {
      coordinates: {lat: 53.44227, lng: 14.55332},
      info: 'Big Wall Szczecin - Księdza Stanisława Staszica 1, 71-515 Szczecin'
    },
    {
      coordinates: {lat: 53.45494, lng: 14.55132},
      info: 'Bio Korona - Przyjaciół Żołnierza 128B, 71-760 Szczecin'
    },
    {
      coordinates: {lat: 53.42514, lng: 14.55266},
      info: 'Bistro na Językach - Grodzka 2, 70-560 Szczecin'
    },
    {
      coordinates: {lat: 53.4323, lng: 14.5695},
      info: 'Bona Gustus - Storrady Świętosławy 1c, 71-602 Szczecin'
    },
    {
      coordinates: {lat: 53.4314, lng: 14.5458},
      info: 'Bro Burgers - Generała Ludomiła Rayskiego 24, 70-442 Szczecin'
    },
    {
      coordinates: {lat: 53.44254, lng: 14.50249},
      info: 'CHECK ALL Burgers & Coffee - Joachima Lelewela 1, 71-154 Szczecin'
    },
    {
      coordinates: {lat: 53.44254, lng: 14.50249},
      info: 'Chmiel Świat Piwa - Duńska 74 B, 70-795 Szczecin'
    },
    {
      coordinates: {lat: 53.42786, lng: 14.55023},
      info: 'Corona Cafe - Kaszubska 67, 71-401 Szczecin'
    },
    {
      coordinates: {lat: 53.419264, lng: 14.552216},
      info: 'Corona Cafe - Kolumba, 70-001 Szczecin (Szczecin Główny)'
    },
    {
      coordinates: {lat: 53.4252, lng: 14.55274},
      info: 'Cukiernia Francuzka - Grodzka 2, 70-560 Szczecin'
    },
    {
      coordinates: {lat: 53.4273, lng: 14.5307},
      info: 'Dary Zdrowia - al. Bohaterów Warszawy 40, 70-342 Szczecin'
    },
    {
      coordinates: {lat: 53.43172, lng: 14.54808},
      info: 'Dzień dobry - Śląska 12, 70-435 Szczecin'
    },
    {
      coordinates: {lat: 53.44192, lng: 14.51194},
      info: 'Dżus Luz Ryneczek Pogodno - Reymonta 3, pawilon 86'
    },
    {
      coordinates: {lat: 53.43831, lng: 14.54536},
      info: 'Eko Vitae - ul. Zygmunta Felczaka 20C, 71-417 Szczecin'
    },
    {
      coordinates: {lat: 53.4272, lng: 14.55245},
      info: 'Eko Life - aleja Niepodległości 28, 70-412 Szczecin'
    },
    {
      coordinates: {lat: 53.43223, lng: 14.54423},
      info: 'Elysium - Monte Cassino 5, 71-899 Szczecin'
    },
    {
      coordinates: {lat: 53.43319, lng: 14.5102},
      info: 'Filipinka - Stanisława Ignacego Witkiewicza 1B, 71-121 Szczecin'
    },
    {
      coordinates: {lat: 53.44334, lng: 14.55003},
      info: 'Green Essence - Księdza Stanisława Staszica, 71-515 Szczecin'
    },
    {
      coordinates: {lat: 53.42984, lng: 14.55228},
      info: 'Jak Malina - Małopolska 3, 70-413 Szczecin'
    },
    {
      coordinates: {lat: 53.43367, lng: 14.54282},
      info: 'Kisiel. - Monte Cassino 35/2, 71-899 Szczecin'
    },
    {
      coordinates: {lat: 53.48683, lng: 14.38456},
      info: 'Lewiatan - Szczecińska 18, 72-003 Dobra'
    },
    {
      coordinates: {lat: 53.44902, lng: 14.39097},
      info: 'Lubieszyn 21 - Lubieszyn 21, 72-002 Lubieszyn'
    },
    {
      coordinates: {lat: 53.4259, lng: 14.53374},
      info: 'Manufaktura Wakame - Sikorskiego 8 wejście od ul.Pułaskiego, 70-313 Szczecin'
    },
    {
      coordinates: {lat: 53.42337, lng: 14.56444},
      info: 'Ole Bistro & Tapas - Bulwar Gdyński 2, Zbożowa 4, 70-653 Szczecin'
    },
    {
      coordinates: {lat: 53.42927, lng: 14.55023},
      info: 'Organic Garden - ul. Kaszubska 4/1, 70-403 Szczecin'
    },
    {
      coordinates: {lat: 53.42927, lng: 14.55023},
      info: 'Organic Garden - ul. Modra 118, 71-220 Szczecin'
    },
    {
      coordinates: {lat: 53.455790, lng: 14.534924},
      info: 'Owoce Warzywa C.H. Chopina - Chopina dz. 13/18, 71-450 Szczecin'
    },
    {
      coordinates: {lat: 53.39937, lng: 14.49117},
      info: 'Papaj Crossfit - Husarów 4A, 71-005 Szczecin'
    },
    {
      coordinates: {lat: 53.43701, lng: 14.54822},
      info: 'Pleciuga Caffee - pl. Teatralny 1, 71-403 Szczecin'
    },
    {
      coordinates: {lat: 53.44984, lng: 14.48333},
      info: 'Piekarnia ul. Szeroka - Szeroka 40, 71-211 Szczecin'
    },
    {
      coordinates: {lat: 53.45491, lng: 14.56239},
      info: 'Prosto z działki - CH Wilcza, Przyjaciół Żołnierza 4, 70-953 Szczecin'
    },
    {
      coordinates: {lat: 53.42979, lng: 14.55261},
      info: 'Protein Point - Małopolska 4, 70-513 Szczecin'
    },
    {
      coordinates: {lat: 53.39718, lng: 14.49392},
      info: 'Protein Point - Eugeniusza Kwiatkowskiego 1a/1 71-004 Szczecin'
    },
    {
      coordinates: {lat: 53.47576, lng: 14.43945},
      info: 'Przystanek - Lipowa 51a, 72-003 Wołczkowo'
    },
    {
      coordinates: {lat: 53.43184, lng: 14.54588},
      info: 'Przystań na Kawę - Generała Ludomiła Rayskiego 19, 70-442 Szczecin'
    },
    {
      coordinates: {lat: 53.44433, lng: 14.40258},
      info: 'Restauracja Przytulna - Bławatkowa 2, 72-002 Dołuje'
    },
    {
      coordinates: {lat: 53.42776, lng: 14.54522},
      info: 'Restauracja Sowa - aleja Wojska Polskiego 17, 70-470 Szczecin'
    },
    {
      coordinates: {lat: 53.42823, lng: 14.54382},
      info: "Roll'n'go centrum - aleja Wojska Polskiego 20, 70-471 Szczecin"
    },
    {
      coordinates: {lat: 53.442146, lng: 14.511912},
      info: "Roll'n'go Ryneczek Pogodno - Ul. Reymonta 3, 71-276 Szczecin"
    },
    {
      coordinates: {lat: 53.44316, lng: 14.55069},
      info: 'Samo Zdrowie - Staszica 1, 71-520 Szczecin'
    },
    {
      coordinates: {lat: 53.41155, lng: 14.49614},
      info: 'Sklep Spożywczo-Monopolowy "Anusia" - Krakowska 15/U1, 70-021 Szczecin'
    },
    {
      coordinates: {lat: 53.423671, lng: 14.490962},
      info: 'Sklep Spożywczo-Monopolowy - ul. Kwiatowa 4 (Gumieńce), 71-045 Szczecin'
    },
    {
      coordinates: {lat: 53.459969, lng: 14.535414},
      info: 'Sklep Spożywczo-Monopolowy "Na Stoku" - Wiosny Ludów, 71-457 Szczecin'
    },
    {
      coordinates: {lat: 53.47308, lng: 14.53865},
      info: 'Sklep Spożywczo-Monopolowy "Natka" ul. Wapienna 1/7, 71-790 Szczecin'
    },
    {
      coordinates: {lat: 53.44247, lng: 14.51205},
      info: 'Sklep Zielarsko-Medyczny Ryneczek Pogodno - Reymonta 3, 71-276 Szczecin'
    },
    {
      coordinates: {lat: 53.44213, lng: 14.51193},
      info: 'Sklep Ogólnospożywczy Ryneczek Pogodno - Reymonta 3, 71-276 Szczecin'
    },
    {
      coordinates: {lat: 53.39695, lng: 14.49444},
      info: 'Smak Natury - Ul. Kwiatkowskiego1/8, 71-004 Szczecin'
    },
    {
      coordinates: {lat: 53.38129, lng: 14.65742},
      info: 'Smak Życia - Rydla 68 C, 70-001 Szczecin'
    },
    {
      coordinates: {lat: 53.45267, lng: 14.53847},
      info: 'Studium Dobrego Piwa - Fryderyka Chopina 55, 71-541 Szczecin'
    },
    {
      coordinates: {lat: 53.42568, lng: 14.534},
      info: 'Szczecińska Beza - Sikorskiego 8, 70-313 Szczecin'
    },
    {
      coordinates: {lat: 53.45037, lng: 14.53623},
      info: 'Technobar - Cyfrowa 6, 71-441 Szczecin'
    },
    {
      coordinates: {lat: 53.42305, lng: 14.56097},
      info: 'The Kitchen Meet & Eat - bulwar Piastowski 3, 70-545 Szczecin'
    },
    {
      coordinates: {lat: 53.42383, lng: 14.56003},
      info: 'The Office - Craft Beer Pub - Osiek 10, 70-535 Szczecin'
    },
    {
      coordinates: {lat: 53.42793, lng: 14.5534},
      info: 'Tkacka Kuchnia Kraftowa - 2a, Tkacka, 70-561 Szczecin'
    },
    {
      coordinates: {lat: 53.4423, lng: 14.51229},
      info: 'Vinoteka - Reymonta 3/pawilon 82, 70-953 Szczecin'
    },
    {
      coordinates: {lat: 53.40701, lng: 14.51761},
      info: 'Vinoteka - Milczańska 31f, 70-117 Szczecin'
    },
    {
      coordinates: {lat: 53.43316, lng: 14.5429},
      info: 'Vege Club Amar - Monte Cassino 36a, 70-001 Szczecin'
    },
    {
      coordinates: {lat: 53.42469, lng: 14.54851},
      info: 'Viki Victoria Cafe - Plac Zwycięstwa 5A, 70-233 Szczecin'
    },
    {
      coordinates: {lat: 53.4836, lng: 14.54294},
      info: 'Weranda - Podbórzańska 54, 71-784 Szczecin'
    },
    {
      coordinates: {lat: 53.42432, lng: 14.56018},
      info: 'Wyszak Browar Rodzinny - Księcia Mściwoja II 8, 70-535 Szczecin'
    },
    {
      coordinates: {lat: 53.45387, lng: 14.56278},
      info: 'Zdrowy Zakątek - Przyjaciół Żołnierza 4/4B/4, 71-670 Szczecin'
    },
    {
      coordinates: {lat: 53.44313, lng: 14.5119},
      info: 'Zielony Kram - Reymonta 5, 71-276 Szczecin'
    },
    {
      coordinates: {lat: 53.43505, lng: 14.56835},
      info: 'Żabka - Pl. Zycięstwa 4-5, 76-020 Szczecin'
    },
    {
      coordinates: {lat: 53.4791, lng: 14.54188},
      info: 'Żabka - ul. Jaworzynki 6, 71-784 Szczecin'
    },
    {
      coordinates: {lat: 53.42877, lng: 14.47688},
      info: 'Żabka - ul. Spółdzielców 21C, 72-006 Mierzyn'
    },
    {
      coordinates: {lat: 53.42947, lng: 14.46585},
      info: 'Żabka - ul. Welecka 38, 72-006 Mierzyn'
    },
    {
      coordinates: {lat: 52.405784, lng: 16.932109},
      info: 'Bliżej Natury, Podgórna 19, 61-828 Poznań',
    },{
      coordinates: {lat: 52.413575, lng: 16.905371},
      info: 'Drink Shop, Poznańska 7, 60-860 Poznań',
    },{
      coordinates: {lat: 52.405861, lng: 16.927106},
      info: 'Eat Sweet & Stay Fit, Piekary 25, 61-823 Poznań',
    },{
      coordinates: {lat: 52.414919, lng: 16.908335},
      info: 'EkoKram, Mylna 23/11, 60-856 Poznań',
    },{
      coordinates: {lat: 52.397472, lng: 16.904587},
      info: 'Restauracja Powolność, Kanałowa 15, 60-710 Poznań',
    },{
      coordinates: {lat: 52.400234, lng: 16.906619},
      info: 'Roślinny Skład, Głogowska 27/3, 60-702 Poznań',
    },{
      coordinates: {lat: 52.435255, lng: 16.927492},
      info: 'Smaki Świata, Aleje Solidarności 42, 61-696 Poznań',
    },{
      coordinates: {lat: 52.409240, lng: 16.822666},
      info: 'Spiżarnia Poznańska, Złotowska 55, 60-184 Poznań',
    },{
      coordinates: {lat: 52.429831, lng: 16.928542},
      info: 'Zdrowie i Tradycja, Słowiańska 40G, 61-664 Poznań',
    },{
      coordinates: {lat: 52.395631, lng: 16.924144},
      info: 'Zielona Micha, Górna Wilda 89, 61-567 Poznań',
    },{
      coordinates: {lat: 52.761838, lng: 15.252245},
      info: 'Matka Natura, plac Jana Pawła II, 66-400 Gorzów Wielkopolski',
    },{
      coordinates: {lat: 52.735678, lng: 15.232150},
      info: 'Piramida Sana - Armii Polskiej 22, 66-400 Gorzów Wielkopolski',
    },{
      coordinates: {lat: 52.199219, lng: 21.021794},
      info: 'Bazar Olkulska Sklep BIO - Olkuska 12, 02-604 Warszawa',
    },{
      coordinates: {lat: 52.224558, lng: 20.942191},
      info: 'Owoce i Warzywa Aleksandra Rułka - ul. Jana Kazimierza 32/U4, 01-308 Warszawa',
    },{
      coordinates: {lat: 52.224128, lng: 21.095274},
      info: 'Organiczny Raj - Eugeniusza Kwiatkowskiego 12b, 03-984 Warszawa',
    },{
      coordinates: {lat: 52.224779, lng: 20.944285},
      info: 'Sklep Ekologiczny "Wasze Zdrowie" - Jana Kazimierza 30, 01-248 Warszawa',
    },{
      coordinates: {lat: 52.203352, lng: 21.035234},
      info: 'Rolnik Ekologiczny - Turecka 1, 00-745 Warszawa',
    },{
      coordinates: {lat: 52.238992, lng: 20.997447},
      info: 'Sklep Ekologiczny Trawa - plac Mirowski 1, 00-138 Warszawa',
    },{
      coordinates: {lat: 52.280887, lng: 20.952224},
      info: 'Jadłostacja - Kasprowicza 48, 01-871 Warszawa',
    },{
      coordinates: {lat: 52.188447, lng: 20.901017},
      info: 'Sklep Spiżarnia Zdrowia - gen. Felicjana Sławoja Składkowskiego 4, 02-497 Warszawa',
    },{
      coordinates: {lat: 52.212527, lng: 20.955674},
      info: 'Sklep Spiżarnia Zdrowia - BLUE CITY, al. Jerozolimskie 179, 02-222 Warszawa',
    },{
      coordinates: {lat: 52.222150, lng: 21.010924},
      info: 'Smak Natury Delikatesy Ekologiczne - ul. Koszykowa 63, 00-667 Warszawa',
    },{
      coordinates: {lat: 52.134765, lng: 21.062004},
      info: 'Smak Natury Delikatesy Ekologiczne - al. Komisji Edukacji Narodowej 19, 02-797 Warszawa',
    },{
      coordinates: {lat: 52.129301, lng: 21.009932},
      info: 'PyszneEko.pl - ul. Farbiarska 69, 02-862 Warszawa',
    },{
      coordinates: {lat: 52.241971, lng: 21.102032},
      info: 'Smaki świata - ul. Zamieniecka 90 paw. 344, 04-158 Warszawa',
    },{
      coordinates: {lat: 52.258544, lng: 20.994651},
      info: 'Kiosk Stacja Metra Dworzec Gdański - ul. Zygmunta Słomińskiego 6 lok 1014, 03-991 Warszawa',
    },{
      coordinates: {lat: 52.172434, lng: 21.025118},
      info: 'Kiosk Stacja Metra Służew - ul. Wałbrzyska 11 lok 112, 02-739 Warszawa',
    },{
      coordinates: {lat: 52.255729, lng: 20.922140},
      info: 'Delikatesy Dr. Gaja - ul. Wrocławska 21, 01-493 Warszawa',
    },{
      coordinates: {lat: 52.199219, lng: 21.021794},
      info: 'Planetarianie - ul. Olkuska 12, 02-604 Warszawa',
    }
  ];

  const geoData = {
    type: 'FeatureCollection',
    features: places.map(place => ({
      type: 'Feature',
      properties: {
        message: place.info,
        iconSize: [30, 30],
        icon: {
          url: `/images/pin.png`
        }
      },
      geometry: {
        type: 'Point',
        coordinates: [place.coordinates.lng, place.coordinates.lat]
      }
    }))
  };

  geoData.features.forEach(marker => {
    const el = document.createElement('div');
    el.className = 'marker';
    el.style.backgroundImage = `url(${marker.properties.icon.url})`;
    el.style.width = marker.properties.iconSize[0] + 'px';
    el.style.height = marker.properties.iconSize[1] + 'px';

    const popup = new mapboxgl.Popup()
      .setText(marker.properties.message);

    // add marker to map
    new mapboxgl.Marker(el)
      .setLngLat(marker.geometry.coordinates)
      .setPopup(popup)
      .addTo(map);
  });

  map.on('load', function() {
    var layers = map.getStyle().layers;

    var labelLayerId;
    for (var i = 0; i < layers.length; i++) {
      if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
        labelLayerId = layers[i].id;
        break;
      }
    }

    map.addLayer({
      'id': '3d-buildings',
      'source': 'composite',
      'source-layer': 'building',
      'filter': ['==', 'extrude', 'true'],
      'type': 'fill-extrusion',
      'minzoom': 15,
      'paint': {
        'fill-extrusion-color': '#aaa',
        'fill-extrusion-height': [
          "interpolate", ["linear"], ["zoom"],
          15, 0,
          15.05, ["get", "height"]
        ],
        'fill-extrusion-base': [
          "interpolate", ["linear"], ["zoom"],
          15, 0,
          15.05, ["get", "min_height"]
        ],
        'fill-extrusion-opacity': .6
      }
    }, labelLayerId);
  });
})(window.mapboxgl);
